package com.zbw.wesley_ui.compose;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zbw.wesley_ui.R;

/**
 * 作者 ：Wesley
 * 时间 ：2020-04-16 16:04
 * 这个类是干嘛的？：ComposeLayout
 */
public class ComposeLayout extends RelativeLayout {
    private View mView;
    private ItemOnClickListener itemOnClickListener;
    private RelativeLayout compose_rl;
    private RelativeLayout compose_left_rl;
    private RelativeLayout image_right_rl;
    private TextView text_tv;
    private ImageView image_left_iv;
    private EditText text_et;
    private ImageView image_right_iv;

    public ComposeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * 初始化
     *
     * @param context 上下文对象
     * @param attrs   AttributeSet
     */
    private void init(Context context, AttributeSet attrs) {
        initView(context);
        initAttrs(context, attrs);
    }

    /**
     * 初始化UI
     *
     * @param context 上下文对象
     */
    private void initView(Context context) {
        mView = LayoutInflater.from(context).inflate(R.layout.compose_layout, null);
        compose_rl = mView.findViewById(R.id.compose_rl);
        compose_left_rl = mView.findViewById(R.id.compose_left_rl);
        image_right_rl = mView.findViewById(R.id.image_right_rl);
        text_tv = mView.findViewById(R.id.text_tv);
        image_left_iv = mView.findViewById(R.id.image_left_iv);
        text_et = mView.findViewById(R.id.text_et);
        image_right_iv = mView.findViewById(R.id.image_right_iv);
        addView(mView);
        compose_rl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemOnClickListener != null) {
                    itemOnClickListener.onClick(v);
                }
            }
        });
        image_right_rl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemOnClickListener != null) {
                    itemOnClickListener.onClick(v);
                }
            }
        });
        text_et.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemOnClickListener != null) {
                    itemOnClickListener.onClick(v);
                }
            }
        });
    }

    /**
     * 输入框能否输入
     * @param enabled true 可以输入
     *                false 不可以输入
     */
    public void setEnabled(boolean enabled){
        if(enabled){
            text_et.setFocusable(true);
            compose_rl.setBackgroundResource(R.drawable.compose_bg);
        }else{
            text_et.setFocusable(false);
            compose_rl.setBackgroundResource(R.drawable.compose_bg1);
        }
    }

    /**
     * 设置Edittext
     *
     * @param text 输入文本
     */
    public void setText(String text) {
        text_et.setText(text);
    }

    /**
     * @return 返回Edittext 内容
     */
    public String getText() {
        return text_et.getText().toString().trim();
    }


    /**
     * 获取XML中的赋值
     */
    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ComposeLayout);
        //左边小红星是否显示（必填项）默认false 不显示
        boolean leftImageShow = typedArray.getBoolean(R.styleable.ComposeLayout_leftImageShow, false);
        //右边图片是否显示 默认false 不显示
        boolean rightImageShow = typedArray.getBoolean(R.styleable.ComposeLayout_rightImageShow, false);
        //左边RelativeLayout 布局宽度，防止里面textview 长度不够，默认120
        float leftRL_width = typedArray.getDimension(R.styleable.ComposeLayout_leftRL_width, 0f);
        //EditText输入框是否可以输入，默认是ture 可以输入
        boolean et_focus = typedArray.getBoolean(R.styleable.ComposeLayout_et_focus, true);
        //EditText输入框字符类型
        int et_inputType = typedArray.getInt(R.styleable.ComposeLayout_inputType, -1);
        //左边RelativeLayout 布局背景图片
        Drawable leftRL_bg = typedArray.getDrawable(R.styleable.ComposeLayout_leftRL_bg);
        //右边Image 背景图片
        Drawable rightImage_bg = typedArray.getDrawable(R.styleable.ComposeLayout_rightImage_bg);
        //左边textview 填充内容
        String leftTx_text = typedArray.getString(R.styleable.ComposeLayout_leftTx_text);
        //左边textview 字体颜色
        int leftTx_color = typedArray.getColor(R.styleable.ComposeLayout_leftTx_color, -1);
        //左边textview 字体大小，默认20sp
        float leftTx_size = typedArray.getDimension(R.styleable.ComposeLayout_leftTx_size, -1f);

        //右边Edittext 字体颜色
        int rightEt_color = typedArray.getColor(R.styleable.ComposeLayout_rightEt_color, -1);

        typedArray.recycle();

        if (leftImageShow) {
            image_left_iv.setVisibility(VISIBLE);
        }

        if (rightImageShow) {
            image_right_iv.setVisibility(VISIBLE);
            image_right_rl.setVisibility(VISIBLE);
        }

        if (leftRL_width != 0f) {
            compose_left_rl.setLayoutParams(new RelativeLayout.LayoutParams((int) leftRL_width, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        if (!et_focus) {
            text_et.setFocusable(false);
            compose_rl.setBackgroundResource(R.drawable.compose_bg1);
        }

        if(rightEt_color != -1){
            text_et.setTextColor(rightEt_color);
        }

        if (et_inputType != -1) {
            text_et.setInputType(et_inputType);
        }

        if (leftRL_bg != null) {
            compose_left_rl.setBackground(leftRL_bg);
        }

        if (rightImage_bg != null) {
            image_right_iv.setBackground(rightImage_bg);
        }

        text_tv.setText(leftTx_text);

        if (leftTx_color != -1) {
            text_tv.setTextColor(leftTx_color);
        }

        if (leftTx_size != -1f) {
            text_tv.setTextSize(leftTx_size);
        }

    }


    /**
     * 接口回调
     */
    public interface ItemOnClickListener {
        void onClick(View v);
    }


    public void setItemOnClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

}
