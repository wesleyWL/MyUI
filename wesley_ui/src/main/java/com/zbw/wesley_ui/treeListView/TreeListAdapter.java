package com.zbw.wesley_ui.treeListView;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zbw.wesley_ui.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2020-05-15 11:52
 * 这个类是干嘛的？：TreeListAdapter
 */
public class TreeListAdapter extends TreeListViewAdapter {
    /**
     * @param mTree              view
     * @param context            上下文对象
     * @param data              数据
     * @param defaultExpandLevel 默认展开几级树
     * @param isHide             选择框是否隐藏
     */
    public TreeListAdapter(ListView mTree, Context context, List<Node> data, int defaultExpandLevel, boolean isHide) throws IllegalArgumentException, IllegalAccessException {
        super(mTree, context, data, defaultExpandLevel, isHide);
    }

    @Override
    public View getConvertView(final Node node, final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.adapter_treelist_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.treeNodeIcon_iv =  convertView.findViewById(R.id.treeNodeIcon_iv);
            viewHolder.treeNodeName_tv =  convertView.findViewById(R.id.treeNodeName_tv);
            viewHolder.treeNodeCheck_cb = convertView.findViewById(R.id.treeNodeCheck_cb);
            viewHolder.treeNode_rl = convertView.findViewById(R.id.treeNode_rl);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (node.getIcon() == -1){
            viewHolder.treeNodeIcon_iv.setVisibility(View.INVISIBLE);
        } else{
            viewHolder.treeNodeIcon_iv.setVisibility(View.VISIBLE);
            viewHolder.treeNodeIcon_iv.setImageResource(node.getIcon());
        }
        if(node.isHideChecked()){
            viewHolder.treeNodeCheck_cb.setVisibility(View.GONE);
        }else {
            viewHolder.treeNodeCheck_cb.setVisibility(View.VISIBLE);
            setCheckBoxBg(viewHolder.treeNodeCheck_cb, node.isChecked());
            viewHolder.treeNodeCheck_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                    if(TreeOnClickUtil.isFastClick()) {
                        TreeHelper.setNodeChecked(node, isChecked);
                        Log.i("====" + isChecked + position, node.toString());
                        List<Node> checkedNodes = new ArrayList<>();
                        for (Node n : mAllNodes) {
                            if (n.isChecked()) {
                                checkedNodes.add(n);
                            }
                        }
                        if (onTreeNodeClickListener != null) {
                            onTreeNodeClickListener.onCheckChange(node, position, checkedNodes);
                        }
                        notifyDataSetChanged();
                    }
                }
            });
        }
        viewHolder.treeNodeCheck_cb.setChecked(node.isChecked());
        viewHolder.treeNodeName_tv.setText(node.getName());
        if(node.isCheckedItem()){
            viewHolder.treeNodeName_tv.setTextColor(Color.parseColor("#32B0FD"));
        }else{
            viewHolder.treeNodeName_tv.setTextColor(Color.parseColor("#777777"));
        }

        return convertView;
    }

    private void setCheckBoxBg(CheckBox cb, boolean isChecked){
        if(isChecked){
            cb.setBackgroundResource(R.drawable.ui_check_t);
        }else{
            cb.setBackgroundResource(R.drawable.ui_check_f);
        }
    }

    private static final class ViewHolder {
        ImageView treeNodeIcon_iv;
        TextView treeNodeName_tv;
        CheckBox treeNodeCheck_cb;
        RelativeLayout treeNode_rl;
    }
}
