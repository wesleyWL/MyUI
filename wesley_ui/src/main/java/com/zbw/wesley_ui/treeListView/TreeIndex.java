package com.zbw.wesley_ui.treeListView;

/**
 * 作者 ：Wesley
 * 时间 ：2020-05-26 17:05
 * 这个类是干嘛的？：TreeIndex
 */
public class TreeIndex {
    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "TreeIndex{" +
                "index=" + index +
                '}';
    }
}
