package com.zbw.wesley_ui.treeListView;
import com.zbw.wesley_ui.R;
import java.util.ArrayList;
import java.util.List;

public class TreeHelper {
	/**
	 * 根据所有节点获取可见节点
	 * @param allNodes
	 * @return
	 */
	public static List<Node> filterVisibleNode(List<Node> allNodes) {
		List<Node> visibleNodes = new ArrayList<Node>();
		for (Node node : allNodes) {
			// 如果为根节点，或者上层目录为展开状态
			if (node.isRoot() || node.isParentExpand()) {
				setNodeIcon(node);
				visibleNodes.add(node);
			}
		}
		return visibleNodes;
	}

	/**
	 * 获取排序的所有节点
	 *
	 * @param defaultExpandLevel
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public  static List<Node> getSortedNodes(List<Node> data,
                                                int defaultExpandLevel, boolean isHide)
			throws IllegalAccessException, IllegalArgumentException {
		List<Node> sortedNodes = new ArrayList<Node>();
		// 将用户数据转化为List<Node>
		List<Node> nodes = convertData2Nodes(data, isHide);
		// 拿到根节点
		List<Node> rootNodes = getRootNodes(nodes);
		// 排序以及设置Node间关系
		for (Node node : rootNodes) {
			addNode(sortedNodes, node, defaultExpandLevel, 1);
		}
		return sortedNodes;
	}

	/**
	 * 把一个节点上的所有的内容都挂上去
	 */
	private static void addNode(List<Node> nodes, Node node,
                                int defaultExpandLeval, int currentLevel) {
		nodes.add(node);
		if (defaultExpandLeval >= currentLevel) {
			node.setExpand(false);
		}

		if (node.isLeaf())
			return;
		for (int i = 0; i < node.getChildrenNodes().size(); i++) {
			addNode(nodes, node.getChildrenNodes().get(i), defaultExpandLeval,
					currentLevel + 1);
		}
	}

	/**
	 * 获取所有的根节点
	 * @param nodes
	 * @return
	 */
	public static List<Node> getRootNodes(List<Node> nodes) {
		List<Node> rootNodes = new ArrayList<Node>();
		for (Node node : nodes) {
			if (node.isRoot()) {
				rootNodes.add(node);
			}
		}
		return rootNodes;
	}

	/**
	 * @param nodes
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private  static List<Node> convertData2Nodes(List<Node> nodes, boolean isHide)
			throws IllegalAccessException, IllegalArgumentException {
		for (Node node : nodes) {
			node.setHideChecked(isHide);
		}

		/**
		 * 比较nodes中的所有节点，分别添加children和parent
		 */
		for (int i = 0; i < nodes.size(); i++) {
			Node n = nodes.get(i);
			for (int j = i + 1; j < nodes.size(); j++) {
				Node m = nodes.get(j);
				if (n.getId().equals(m.getpId())) {
					n.getChildrenNodes().add(m);
					m.setParent(n);
				} else if (n.getpId().equals(m.getId())) {
					n.setParent(m);
					m.getChildrenNodes().add(n);
				}
			}
		}
		for (Node n : nodes) {
			setNodeIcon(n);
		}
		return nodes;
	}

	/**
	 * 设置打开，关闭icon
	 * 
	 * @param node
	 */
	private static void setNodeIcon(Node node) {
		if (node.getChildrenNodes().size() > 0 && node.isExpand()) {
			node.setIcon(R.drawable.ui_tree_listview_unfold);
		} else if (node.getChildrenNodes().size() > 0 && !node.isExpand()) {
			node.setIcon(R.drawable.ui_tree_listview_collapse);
		} else
			node.setIcon(-1);
	}

	public static void setNodeChecked(Node node, boolean isChecked) {
		//自己设置是否选择
		node.setChecked(isChecked);
		//子节点处理
		setChildrenNodeChecked(node, isChecked);
		//父节点处理
		setParentNodeChecked(node);
	}

	/**
	 * 非叶子节点,子节点处理
	 */
	private static void setChildrenNodeChecked(Node node, boolean isChecked) {
		node.setChecked(isChecked);
		if (!node.isLeaf()) {
			for (Node n : node.getChildrenNodes()) {
				// 所有子节点设置是否选择
				setChildrenNodeChecked(n, isChecked);
			}
		}
	}

	/**
	 * 设置父节点选择
	 * @param node
	 */
	private static void setParentNodeChecked(Node node) {
		if (!node.isRoot()) {
			Node rootNode = node.getParent();
			boolean isAllChecked = false;
			for (Node n : rootNode.getChildrenNodes()) {
				if (n.isChecked()) {
					isAllChecked = true;
					break;
				}
			}
			rootNode.setChecked(isAllChecked);
			setParentNodeChecked(rootNode);
		}
	}

}
