package com.zbw.wesley_ui.treeListView;

/**
 * Created by Administrator on 2018/4/24.
 * 防抖动（极短时间多次点击，导致界面弹出多个dialog）
 */

public class TreeOnClickUtil {

    // 两次点击按钮之间的点击间隔不能少于500毫秒
    private static final int MIN_CLICK_DELAY_TIME = 200;
    private static long lastClickTime;

    public static boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }

}
