package com.zbw.myapplication;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zbw.wesley_ui.swipeMenu.SwipeMenuLayout;
import com.zbw.wesley_ui.toast.XToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2020-01-03 16:49
 * 这个类是干嘛的？：SwipeMenuActivity
 */
public class SwipeMenuActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private List<String> listData;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_menu);



        initIData();
        initIView();
    }


    private void initIView() {
        recyclerView =  findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(R.layout.item_swipe_menu, listData, new Callback() {
            @Override
            public void onClick(int index) {
                listData.remove(index);
                myAdapter.notifyDataSetChanged();
            }
        });
        recyclerView.setAdapter(myAdapter);
        inflater = getLayoutInflater();

    }


    private void initIData() {
        listData = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            listData.add("index is =" + i);
        }
    }


    public class MyAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
        private Callback mCallback;


        public MyAdapter(@LayoutRes int layoutResId, @Nullable List<String> data, Callback Callback) {
            super(layoutResId, data);
            this.mCallback = Callback;
        }

        @Override
        protected void convert(final BaseViewHolder helper, final String item) {
            helper.setText(R.id.id,item);

            helper.getView(R.id.right_menu_2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    XToast.success(SwipeMenuActivity.this,"点击了右边！").show();
                    Log.i("SwipeMenuActivity","点击了右边");
                    SwipeMenuLayout easySwipeMenuLayout = helper.getView(R.id.es);
                    easySwipeMenuLayout.resetStatus();
                }
            });

            helper.getView(R.id.del).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    XToast.error(SwipeMenuActivity.this,"删除"+item).show();
                    Log.i("删除",helper.getLayoutPosition()+"");
//                    SwipeMenuLayout easySwipeMenuLayout = helper.getView(R.id.es);
//                    easySwipeMenuLayout.resetStatus();
                    mCallback.onClick(helper.getLayoutPosition());
                }
            });




            helper.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("SwipeMenuActivity","点击了内容:"+item);
                    XToast.success(SwipeMenuActivity.this,"点击了内容"+item).show();
                }
            });

        }

    }
}

interface Callback{

    void onClick(int index);

}