package com.zbw.myapplication.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zbw.myapplication.R;
import com.zbw.wesley_ui.weSwipe.WeSwipeHelper;


import java.util.List;


/**
 * 作者 ：Wesley
 * 时间 ：2020-01-07 16:35
 * 这个类是干嘛的？：SwipeMenu1Adapter
 */
public class SwipeMenu1Adapter extends BaseQuickAdapter<String, SwipeMenu1Adapter.SwipeViewHolder> {


    public SwipeMenu1Adapter(@Nullable List<String> data) {
        super(R.layout.layout_item,data);
    }

    @Override
    protected void convert(final SwipeViewHolder helper, String item) {
        helper.setText(R.id.text,item);
        helper.addOnClickListener(R.id.item_slide);
        helper.addOnClickListener(R.id.item_text);


//        helper.slide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("删除",""+helper.getAdapterPosition());
//            }
//        });



    }

    public class SwipeViewHolder extends BaseViewHolder implements WeSwipeHelper.SwipeLayoutTypeCallBack{
        public LinearLayout textView;
        public RelativeLayout slide;


        public SwipeViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.item_text);
            slide = view.findViewById(R.id.item_slide);
        }

        @Override
        public float getSwipeWidth() {
            return slide.getWidth();
        }

        @Override
        public View needSwipeLayout() {
            return textView;
        }

        @Override
        public View onScreenView() {
            return textView;
        }
    }

}
