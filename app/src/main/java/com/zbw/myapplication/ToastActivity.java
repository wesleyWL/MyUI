package com.zbw.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.zbw.wesley_ui.toast.XToast;

/**
 * 测试自定义Toast
 */
public class ToastActivity extends AppCompatActivity {

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToast.normal(ToastActivity.this,"默认toast!").show();
            }
        });
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToast.success(ToastActivity.this,"交易成功！").show();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToast.error(ToastActivity.this,"交易失败！").show();
            }
        });
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToast.warning(ToastActivity.this,"交易异常！请检查参数是否正确").show();
            }
        });
        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToast.info(ToastActivity.this,"当前无法连接到网络！").show();
            }
        });
        //测试git push
    }
}
