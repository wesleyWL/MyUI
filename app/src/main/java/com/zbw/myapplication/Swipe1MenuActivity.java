package com.zbw.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.zbw.myapplication.adapter.SwipeMenu1Adapter;
import com.zbw.wesley_ui.weSwipe.WeSwipe;

import java.util.ArrayList;
import java.util.List;



/**
 * 作者 ：Wesley
 * 时间 ：2020-01-07 16:31
 * 这个类是干嘛的？：Swipe1MenuActivity
 */
public class Swipe1MenuActivity extends Activity {
    private RecyclerView recyclerView;
    private SwipeMenu1Adapter mAdapter;
    private List<String> listData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_menu);
        recyclerView =  findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        listData = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            listData.add("index is =" + i);
        }
        mAdapter = new SwipeMenu1Adapter(listData);
        recyclerView.setAdapter(mAdapter);
        WeSwipe.attach(recyclerView);

        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.item_slide:
                        Log.i("删除", "" + position);
//                        if (position >= 0 && position < listData.size()) {
//                            listData.remove(position);
//                            mAdapter.notifyItemRemoved(position);
//                        }
                        break;

                    case R.id.item_text:
                        Log.i("点击", "" + position);
                        break;
                }
            }
        });

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Log.i("点击111",""+position);
            }
        });

    }
}
