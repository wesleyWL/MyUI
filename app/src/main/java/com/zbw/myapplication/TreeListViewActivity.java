package com.zbw.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import com.zbw.wesley_ui.toast.XToast;
import com.zbw.wesley_ui.treeListView.Node;
import com.zbw.wesley_ui.treeListView.TreeListAdapter;
import com.zbw.wesley_ui.treeListView.TreeListViewAdapter;


import java.util.ArrayList;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2020-05-15 14:08
 * 这个类是干嘛的？：TreeListViewActivity
 */
public class TreeListViewActivity extends Activity {
     TreeListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treelist);
        ListView listView = findViewById(R.id.listView);
        final List<Node> mData = new ArrayList<>();
        mData.add(new Node("1","0","一级菜单1","001"));
        mData.add(new Node("2","0","一级菜单2","002"));
        mData.add(new Node("3","0","一级菜单3","003"));
        mData.add(new Node("4","0","一级菜单4","004"));

        mData.add(new Node("5","1","二级菜单5","005"));
        mData.add(new Node("6","2","二级菜单6","006"));
        mData.add(new Node("7","3","二级菜单7","007"));
        mData.add(new Node("8","4","二级菜单8","008"));

        mData.add(new Node("9","1","二级菜单9","009"));
        mData.add(new Node("10","5","三级菜单10","0010"));
        mData.add(new Node("11","5","三级菜单11","0011"));
        mData.add(new Node("12","5","三级菜单12","0011"));
        mData.add(new Node("13","5","三级菜单13","0011"));
        mData.add(new Node("14","5","三级菜单14","0011"));

        try {
            adapter = new TreeListAdapter(listView,this,mData,10,false);
            adapter.setOnTreeNodeClickListener(new TreeListViewAdapter.OnTreeNodeClickListener() {
                @Override
                public void onClick(Node node, int position) {

                }

                @Override
                public void onCheckChange(Node node, int position, List<Node> checkedNodes) {
                    XToast.success(TreeListViewActivity.this, "选中的长度"+checkedNodes.size()).show();
                }
            });

            listView.setAdapter(adapter);


//            adapter.addNode(new Node("12","1","测试新增","012"));
//            adapter.addNode(new Node("13","5","测试新增1","013"));

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public void additem(View v){
        adapter.addNode(new Node("15","3","测试新增","012"));
    }


}
