package com.zbw.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.zbw.wesley_ui.compose.ComposeLayout;
import com.zbw.wesley_ui.compose.ComposeLayout.ItemOnClickListener;
import com.zbw.wesley_ui.toast.XToast;

/**
 * 作者 ：Wesley
 * 时间 ：2020-01-03 16:43
 * 这个类是干嘛的？：MainActivity
 */
public class MainActivity extends AppCompatActivity {
    private ComposeLayout mLayout;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        XToast.Config.get()
                .setAlpha(200)
                .allowQueue(false);

        mLayout = findViewById(R.id.compose);
        mLayout.setText("输入框测试");
        mLayout.setEnabled(false);
        mLayout.setItemOnClickListener(new ItemOnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.compose_rl:
                        System.out.println("控件点击");
                        break;
                    case R.id.image_right_iv:
                        System.out.println("右侧图片点击");
                        break;
                    case R.id.text_et:
                        System.out.println("点击输入框");
                        break;
                }
            }
        });



    }

    public void toToast(View V){
        startActivity(new Intent(this,ToastActivity.class));
    }

    public void toSwipeMenu(View V){
        startActivity(new Intent(this,SwipeMenuActivity.class));
    }

    public void toSwipeMenu1(View V){
        startActivity(new Intent(this,Swipe1MenuActivity.class));
    }

    public void toTreeList(View V){
        startActivity(new Intent(this,TreeListViewActivity.class));
    }

}
